# First Person Controller

Add the `PlayerController.cs` script to the player body
Add the `LookController.cs` script to the first person camera.
Make sure to add an empty gameobject to the player body, and put it at the feet.
Add appropriate components to the `PlayerController` and `LookController`
Download and install `New Input System`
Restart your project.
Look at `PlayerInputs.inputactions` in the inspector and make sure it's generating a C# script.
???
Profit