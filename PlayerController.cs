﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.InputSystem; //Uncomment if using 2019.2 or later.

[RequireComponent(typeof(CharacterController))]
public class PlayerController : MonoBehaviour
{
    //A reference to our character controller.
    private CharacterController controller;
    private PlayerInputs inputs = null;
    //public InteractionBehaviour interactionTarget = null;

    #region Variables
    [Header("Movement Variables")]
    //Move speed. Meters per second. Currently set to average jog speed of a human.
    [SerializeField] private float moveSpeed = 3.7f;
    //Sprint speed. Also meters per second. Currently set to average sprint speed of a human.
    [SerializeField] private float sprintSpeed = 7.69f;
    //Jump hight in meters. Currently set to "Very Good" average of human scale. Like the NBA or something.
    [SerializeField] private float jumpHeight = 0.7112f;
    //Gravity. It's a constant. Currently set to 'real' gravity. (9.81m/s)
    [SerializeField] private float gravity = -9.8321849378f;
    //Velocity vector to help us calculate gravity realistically.
    Vector3 velocity;
    Vector3 movement;
    //A boolean to determine if we're sprinting or not.
    private bool isSprinting;
    [SerializeField] private float gravityBuffer = -2f;
    public bool onLadder = false;
    public bool isTouching;
    #endregion

    private void Awake() {
        inputs = new PlayerInputs();
        inputs.Player.Jump.performed += ctx => Jump();
        inputs.Player.Sprint.performed += ctx => isSprinting = true;
        inputs.Player.Sprint.canceled += ctx => isSprinting = false;
        inputs.Player.Interact.performed += ctx => Interact();

    }
    // Start is called before the first frame update
    void Start() {
        controller = GetComponent<CharacterController>();
    }


    // Update is called once per frame
    void Update() {
        //Calculates movement vector.
        CalculateMovement();
    }

    //What happens when we hit the jump button
    void Jump() {
        if (controller.isGrounded)
            velocity.y = Mathf.Sqrt(jumpHeight * -2 * gravity);
    }

    //Calculate our movement vector.
    void CalculateMovement() {
        var movementInput = inputs.Player.Move.ReadValue<Vector2>();
        movement.x = movementInput.x;
        movement.z = movementInput.y;
        movement = movement.normalized;

        //If we're on the ground, set Y velocity to a gravity buffer of some sort to keep us on the ground.
        if (controller.isGrounded && velocity.y < 0) {
            velocity.y = gravityBuffer;
        }
        Vector3 motion = new Vector3();
        //We use transform.right and transform.forward constants to make the
        //Motion vector move relative to our rotation.
        if (!onLadder) {
            motion = transform.right * movement.x + transform.forward * movement.z;
        } else if (onLadder) {
            motion = transform.right * movement.x + transform.up * movement.z;
            velocity.y = 0;
        }
        if (!isSprinting) {
            //We apply our motion vector to the controller, and multiply it by Time.deltaTime to make it smoother.
            controller.Move(motion * moveSpeed * Time.deltaTime);
        } else if (isSprinting) {
            controller.Move(motion * sprintSpeed * Time.deltaTime);
        }

        //We then apply our gravity to the velocity, multiplying by deltaTime to make it not framerate dependant.
        velocity.y += gravity * Time.deltaTime;

        //We then apply the gravity to our movement, multiplying again by deltaTime because physics.
        //Gravity = 9.81m/s/s^2
        controller.Move(velocity * Time.deltaTime);

    }

    void Interact() {
        if (isTouching) {
            //interactionTarget.GetComponent<InteractionBehaviour>().Interact();
            //Debug.Log(interactionTarget.gameObject.name);
            Debug.Log("Interacting with things!");
        }
    }


    private void OnEnable() {
        inputs.Player.Enable();
    }
    private void OnDisable() {
        inputs.Player.Disable();
    }
}
